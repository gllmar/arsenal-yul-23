# Arsenal YUL 2023

![Simulation Arsenal YUL 2023](docs/sr_ensemble_pos_1.png)


## [Radiances](docs/radiances/)

[![simulation radiances](docs/radiances/simulation_radiances.png)](docs/radiances/)

## [Inflorescences](docs/inflorescences/)

[![Simulation Inflorescence](docs/inflorescences/simulation_inflorescences.png)](docs/inflorescences/)


## [Objets-monde](docs/om/)

[![Simulation Objets-monde](docs/om/simulation_om.png)](docs/om/)


## [Vidéos Variées](docs/videovariees/)

[![Simulation video variés](docs/videovariees/simulation_videovariees.png)](docs/videovariees/)


## [Terraforma](docs/terraforma/)

[![Simulation Terraforma](docs/terraforma/simulation_terrraforma.png)](docs/terraforma/)



## [Distributed Memories](docs/dm/)

[![Simulation Distributed Memories](docs/dm/simulation_dm.png)](docs/dm/)


## [House of Skin](docs/hos/)

[![Simulation House of Skin](docs/hos/simulation_hos.png)](docs/hos/)


## [Monades](docs/monades/)
[![Simulation Monade](docs/monades/simulation_monades.png)](docs/monades/)


## [Floralia](docs/floralia/)

[![Simulation Floralia](docs/floralia/simulation_floralia.png)](docs/floralia/)


## [Plantation Technique](docs/arsenal/)

[![Plantation](docs/arsenal/arsenal_plan.drawio.png)](docs/arsenal/)

