# [Arsenal YUL 2023](/)

## Objets-monde

![Simulation](simulation_om.png)

### [Équipement/budget](https://docs.google.com/spreadsheets/d/1PNtwkPkmTvGa8w9PyqVHQ6xDHwXH69XV/edit#gid=1948383822)


### Plantation

![plantation](plantation.drawio.png)

### Élévation

![élévation](elevation.drawio.png)


### Projection 

* 1920x1080, 60fps
* Interactive
* Centrée sur le mur 


### Projecteur

#### LG ProBeam BF50NST

##### Spécifications
* 5000 lumens, 1920x1200 lentille 1.32-2.11

##### Réglages
* Max Wide, Max lens shift vers le bas
* Projection : hauteur : 97" (2460 mm)
* Projection : Largeur : 172" (4370 mm)
* Accrochage : hauteur projection du sol + hauteur projection + lens shift = 
    * 24" + 97" + 48" = 169" 
    * 601 + 2460+ 1220 = 4281 mm
* Lens Shift Max : (1220 mm)
* ((3730 - 2460)/2) 
##### Étude 

![étude projection BF50NST](./om_LG_ProBeam_BF50NST.drawio.png)

##### Liens

* [Achat Amazon](https://www.amazon.ca/LG-BF50NST-Projector-Features-Compliant/dp/B08HH2Z7FF)
* [Manufacturier](https://www.lg.com/ca_en/business/projectors/lg-bf50nst)
* [Manuel](https://gscs-b2c.lge.com/downloadFile?fileId=0OjWA5RIPSkg1aOYgjxa1Q)


<!-- 

Epson PowerLite L520U n'a pas de lens shift


Epson PowerLite L530U Full HD WUXGA Long-Throw Laser Projector - White
https://www.staples.ca/products/24498408-en-epson-powerlite-l530u-full-hd-wuxga-long-throw-laser-projector-white


#### Epson 
Epson Powerlite Pro G7500U
https://www.crawfordssuperstore.com/products/epson-g7500u-wuxga-4k-projector


#### Optoma GT1090HDRx 

* 4200 lumens, 1920x1080, lentille 0.5 fixe
* Projection : Hauteur : 84" (2134 mm)
* Projection : Largeur : 150" (3810 mm)
* Accrochage : Distance de l'écran: 75" (1900 mm) (poutre)
* Accrochage : Hauteur du sol  = 24" + 84" + 13" = 121" (3074 mm) 

### Accrochge

* 1er pôle a environ 6'3" de la surface vidéo
* 2e pôle (6'3" + 16'6" = 22'9" == 273") de la surface vidéo
![étude projection GT1090HDRx](./optoma1090HDRX.drawio.png) 
-->


#### Accrochage

* [PEERLESS UNIVERSAL PROJECTOR MOUNT 217$](https://www.avshop.ca/video/carts-stands-amp-mounts/projector-brackets/peerless-universal-projector-mount)

* [Tige filetée 24"](https://www.avshop.ca/video/carts-stands-amp-mounts/projector-brackets/chief-cms024-24in-fixed-extension-column-black)

* [coupler en guise de EndCap](https://www.avshop.ca/video/carts-stands-amp-mounts/projector-brackets/chief-cma270-npt-threaded-pipe-coupler-black)

* Cheeseborough, si swivel, doubler le nombre

* Tuyaux d'au moins 8 pied

### Connections

``` mermaid
graph LR
    subgraph Floor
    direction LR
        Interactive_interface --Ethernet--> LAN_2[Local Area Network POE]
    end

    subgraph Ceilling
            subgraph ProjectorTruss
            direction LR
            Mac_Mini --> AC1
            ethernet_switch --AC/DC--> AC1
            Mac_Mini --HDMI--> Projector 
            Mac_Mini --Ethernet--> ethernet_switch --> Local_area_network
            Projector --IEC--> AC1[AC 400w]
            Projector --Ethernet--> ethernet_switch
            Mac_Mini --AudioOut_3.5mm--> 3.5mm_To_2_x_6.35mm --> DirectInputBox
        end
        subgraph Speakers
        direction LR
            SpeakerLeft --XLR--> DirectInputBox
            SpeakerLeft --IEC--> AC3[AC 150w]
            SpeakerRight --XLR--> DirectInputBox
            SpeakerRight --IEC--> AC3
        end
    end
```