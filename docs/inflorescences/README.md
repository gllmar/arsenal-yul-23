# [Arsenal YUL 2023](/)
## Inflorescences

![Simulation Inflorescences](simulation_inflorescences.png)

### [Équipement/budget](https://docs.google.com/spreadsheets/d/1PNtwkPkmTvGa8w9PyqVHQ6xDHwXH69XV/edit#gid=1564136649)


### Plantation

![plantation](./plantation.drawio.png)

### Élévation

![élévation](./elevation.drawio.png)




### Projection

* Ratio = 16:9
* Resolution = 1920x1080
* Height = 10' (plancher -> hauteur du mur)
* Width = ~18' (peint noir sur les cotés)
* Seamless loop
* Audio = Stereo 


### Projecteur

#### LG ProBeam BF50NST

![étude-LGProBeam](./inf_LG_ProBeam_BF50NST_imp.png)

équivalent optique au epson PowerLite L530U

#### [Epson PowerLite L570U](https://epson.ca/For-Work/Projectors/Meeting-Room/PowerLite-L570U-3LCD-Laser-Projector-with-4K-Enhancement/p/V11HA98020) 

![étude Epson](./inf_epson_PowerLite_L570U_epson_imp.png)

* 4,094.00 Can ( $3,149 US)


##### Position

* Ceilling mounted
* Lens at 162" (4125mm) from floor
* Lens shift (Vertical): -50%
* Lens shift (Horizontal): 0%


### Connections

``` mermaid
graph LR
    subgraph Floor
    direction LR
        SwitchPOE --IEC--> AC2[AC 150w]
        SwitchPOE --Ethernet--> LAN_2[Local Area Network]
        Sculpture1 --Ethernet-->SwitchPOE
        Sculpture2 --Ethernet-->SwitchPOE
        Sculpture3 --Ethernet-->SwitchPOE
    end

    subgraph Ceilling
            subgraph ProjectorTruss
            direction LR
            Projector --IEC--> AC1[AC 400w]
            Projector --Ethernet--> LAN_1[Local Area Network]
            Projector --AudioOut_3.5mm--> 3.5mm_To_2_x_6.35mm --> DirectInputBox
        end
        subgraph SpeakersTruss
        direction LR
            SpeakerLeft --IEC--> AC3[AC 150w]
            SpeakerLeft --XLR--> DirectInputBox
            SpeakerRight --IEC--> AC3
            SpeakerRight --XLR--> DirectInputBox
        end
    end
```


### Sculptures

#### †echnique

##### Fixation 

* 2x rails din 1m 
* 1x Super clamp
* 2x Vis M5 ~16mm