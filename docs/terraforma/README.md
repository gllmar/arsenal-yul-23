# [Arsenal YUL 2023](/)

## Terraforma

![Simulation](./simulation_terrraforma.png)

## [équipement/budget](https://docs.google.com/spreadsheets/d/1PNtwkPkmTvGa8w9PyqVHQ6xDHwXH69XV/edit#gid=350696072)


## Équipement

## Plantation

![plantation](./plantation.drawio.png)

### Projection sculptures

#### Elevation

![elevation](./elevation.drawio.png)

#### Projection
 
* Width : 100 cm
* Height : 60 cm
* Distance : 
* Projection height : 

#### Projecteurs 

##### Epson Home Cinema 4010

![Epson Home Cinema 4010](./epson_4010.png)

* [Lien Projector Central](https://www.projectorcentral.com/Epson-Home_Cinema_4010.htm)

* [Lien Achat AMZ](https://www.amazon.ca/Epson-Cinema-PRO-UHD-3-Chip-Projector/dp/B07HTNNVBL)

<!--
 ##### Epson Pro EX10000

* 3LCD 1080p Wireless Laser Projector with Miracast

https://www.bestbuy.ca/en-ca/product/epson-pro-ex10000-3lcd-1080p-wireless-laser-projector-with-miracast/15312098


##### Epson PowerLite-L265F

https://epson.ca/For-Work/Projectors/Classroom/PowerLite-L265F-1080p-3LCD-Lamp-Free-Laser-Display-with-Built-In-Wireless/p/V11HA72120 
-->
