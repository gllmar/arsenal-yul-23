# [Arsenal YUL 2023](/)
## Floralia

![Simulation](./simulation_floralia.png)

### [Équipement/budget](https://docs.google.com/spreadsheets/d/1PNtwkPkmTvGa8w9PyqVHQ6xDHwXH69XV/edit#gid=1345950823)

### Plantation

![plantation](./plantation.drawio.png)


### Connections

``` mermaid
graph TD
    subgraph Mur
        Moniteur_1 
        Moniteur_2
        Moniteur_3
        Moniteur_4
    end

    subgraph Speakers
        Speaker_1 --3.5mm vers XLR--> Moniteur_1
        Speaker_2 --3.5mm vers XLR--> Moniteur_2
        Speaker_3 --3.5mm vers XLR--> Moniteur_3
        Speaker_4 --3.5mm vers XLR--> Moniteur_4
    end
```




