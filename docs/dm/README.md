# [Arsenal YUL 2023](/)
## Distributed Memories

![Simulation](./simulation_dm.png)

### [Équipement/budget](https://docs.google.com/spreadsheets/d/1PNtwkPkmTvGa8w9PyqVHQ6xDHwXH69XV/edit#gid=655551499)


### Plantation

![plantation](./plantation.drawio.png)


### Connections

``` mermaid
graph LR
DM[Distributed Memories]
```

### Projection


#### Projecteur


##### Position


### mecanique

#### Stage Clamp 

![Stage Clamp](stage_clamp.drawio.png)

* modification : Filet M12 (1.75?) dans ø M10

#### Adaptateur M12 Male -> 1/4" Femelle

![Adaptateur M12 male -> 1/4" ](adaptateur_m12_1_4.drawio.png))


#### Bras Friction

![friction arm](./friction_arm.drawio.png)