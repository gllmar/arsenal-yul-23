
## Projector Mount

2 produits, relativement équivalent .

* Filet NPT 1 1/2"
* Plaque Quickrelease
* Ajustements "toolless"

### PEERLESS PRG-UNV-W

![projector mount PRG-UNV-W](./PRG-UNV-W.drawio.png)

#### Liens Achat
* [AVSHOP : projector-brackets/peerless-universal-projector-mount 218$ Out of Stock](https://www.avshop.ca/video/carts-stands-amp-mounts/projector-brackets/peerless-universal-projector-mount)

* [AMAZON : Projector Mount Amazon 224.98$](https://www.amazon.ca/PEERLESS-INDUSTRIES-PRGS-UNV-UNIVERSAL-PROJECTORS/dp/B00GYUQT8C)

### PEERLESS PRGS-UNV

![PEERLESS PRGS-UNV](./PRG-UNV.drawio.png)

* [Lien Manufacturier](https://www.peerless-av.com/collections/projector-mounts/products/prgs-unv?variant=34322259378307) 
* [manuel](https://docs.peerless-av.com/BOOK8.5X11IN-056-9041-1-9.1-1.PDF)


#### Lien achat  
* [AVSHop Out of Stock 165$](https://www.avshop.ca/video/carts-stands-amp-mounts/projector-brackets/peerless-universal-projector-mount-p-9736)
* [Vision.ca 220$](https://www.visions.ca/product-detail/32061/peerless_av_prgs_universal_projector_mount?categoryId=0&sku=PRGSUNV)
* []()
* [PC CANADA https://www.pc-canada.com/item/peerless-av-prgs-unv-ceiling-mount-for-projector/prgs-unv](https://www.pc-canada.com/item/peerless-av-prgs-unv-ceiling-mount-for-projector/prgs-unv)


##  Tuyaux Fileté

### Projecteur Horizontal

![](./projector_pole.drawio.png)

### Tuyaux fileté 90 degrés stage (Terraforma) 

![](./thread_pipe_to_stage.drawio.png)


### Référence Standard tuyaux : NPT 1 1/2" 

![](./nptPipe_1.5inch.gif)

![](./npt_1.5_thread.drawio.png)
