

## Long loop

### Mac

```shell
#!/bin/bash

# set input and output filenames
input_file="$1"
loop_count="$2"
output_file="${input_file%.*}_long_loop.${input_file##*.}"

# loop the input video using ffmpeg
ffmpeg -i "$input_file" -filter_complex "loop=$loop_count" -c:a copy "$output_file"

```

### windows

```
@echo off

set input_file=%1
set loop_count=%2
set output_file=%~n1_long_loop%~x1

ffmpeg -i "%input_file%" -filter_complex "loop=%loop_count%" -c:a copy "%output_file%"
```