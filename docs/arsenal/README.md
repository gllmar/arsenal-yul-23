# [Arsenal YUL 2023](/)

## Plantation

![Plantation](./arsenal_plan.drawio.png)

## Connectique


### Électrique

Général
```

```

### Réseau

#### Général
```mermaid
    graph TD
    subgraph T1
        WAN --> switch_arsenal --> AP_Arsenal
        Routeur <-- WAN--> switch_arsenal
        Routeur --LAN--ETH 1'--> switch_T1
        
    end

    subgraph T2
        Routeur -- ETH 50' --> switch_T2
        switch_T2
    end
    subgraph T3
        switch_T2 --ETH 50' --> switch_T3
    end

    subgraph REGIE
        Routeur --> switch_POE_regie --> Computer
    end

```

#### Détail Switch T1 et Régie

```mermaid 
graph LR
    subgraph T1
        Routeur --ETH 3'--> switch_T1
        Routeur <--ETH 6'--> WAN

    end

    subgraph REGIE
        switch_POE_regie --ETH 10'--> Computer   
        Routeur --ETH 50'--> switch_POE_regie

    end

    subgraph TERRAFORMA
        switch_T1 -- ETH 50'--> tf_projector_1[projector_1]
        switch_T1 -- ETH 50' --> tf_mac_mini_1[mac-mini_1]
        switch_T1 -- ETH 50' --> tf_projector_2[projector_2]
        switch_T1 -- ETH 50'--> tf_mac_mini_2[mac-mini_2]
    end 
    subgraph OBJETS-MONDE
        switch_POE_regie --ETH 75'--> om_interface[interactive_interface]
        switch_T1 --ETH 50'--> om_projector[projector]
        switch_T1 --ETH 50'-->  om_mac_mini[mac-mini]
    end
    subgraph INFLORESCENCE
        switch_POE_regie --ETH 25'--> sculpture_1
        switch_POE_regie --ETH 25'--> sculpture_2
        switch_POE_regie --ETH 25'--> sculpture_3
        switch_T1 --ETH 50' --> in_projector[Projector]

    end 


```


#### Détail T2 et T3

```mermaid
    graph LR

    subgraph Distributed_Memories
        subgraph T3
            switch_T3
        end
        subgraph T2
            switch_T2
        end

        switch_T2 --> Projo_1
        switch_T2 --> Pole_1
        switch_T2 --> Pole_2
        switch_T2 --> Pole_3
        switch_T2 --> TV1
        switch_T2 --> TV2
        switch_T3 --> Projo_2
        switch_T3 --> Pole_4
        switch_T3 --> Pole_5
        switch_T3 --> Pole_6
        switch_T3 --> TV_3
        switch_T3 --> TV_4
    
        

    end
    routeur --> switch_T2 --> switch_T3
```

### Audio

### Vidéo

### Clamp

#### Stage clamp
